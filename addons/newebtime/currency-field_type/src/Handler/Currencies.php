<?php

namespace Newebtime\CurrencyFieldType\Handler;

use Newebtime\CurrencyFieldType\CurrencyFieldType;
use Propaganistas\LaravelIntl\Facades\Currency;

/**
 * Class Currencies
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class Currencies
{
    /**
     * Handle the options.
     *
     * @param CurrencyFieldType $fieldType
     */
    public function handle(CurrencyFieldType $fieldType)
    {
        $except = array_get($fieldType->getConfig(), 'except');
        $only   = array_get($fieldType->getConfig(), 'only');

        if ($except) {
            $currencies = array_diff(Currency::all(), $except);

            $fieldType->setOptions($currencies);
        }

        if ($only) {
            $fieldType->setOptions($only);
        }

        if (!$except && !$only) {
            $fieldType->setOptions(Currency::all());
        }
    }
}
