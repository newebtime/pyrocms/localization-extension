<?php

namespace Newebtime\CurrencyFieldType;

use Anomaly\Streams\Platform\Addon\FieldType\FieldType;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Newebtime\CurrencyFieldType\Command\BuildOptions;
use Newebtime\CurrencyFieldType\Handler\Currencies;

/**
 * Class CurrencyFieldType
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class CurrencyFieldType extends FieldType
{
    use DispatchesJobs;

    /**
     * {@inheritdoc}
     */
    protected $class = null;

    /**
     * {@inheritdoc}
     */
    protected $inputView = 'newebtime.field_type.currency::dropdown';

    /**
     * {@inheritdoc}
     */
    protected $config = [
        'except'  => null,
        'only'    => null,
        'handler' => Currencies::class,
    ];

    /**
     * The dropdown options.
     *
     * @var null|array
     */
    protected $options = null;

    /**
     * Get the dropdown options.
     *
     * @return array
     */
    public function getOptions()
    {
        if ($this->options === null) {
            $this->dispatch(new BuildOptions($this));
        }

        return $this->options;
    }

    /**
     * Set the options.
     *
     * @param  array $options
     * @return $this
     */
    public function setOptions(array $options)
    {
        $this->options = $options;

        return $this;
    }

    /**
     * Merge more options on.
     *
     * @param array $options
     * @return $this
     */
    public function mergeOptions(array $options)
    {
        $this->options = array_merge($this->options, $options);

        return $this;
    }

    /**
     * Get the handlers.
     *
     * @return array
     */
    public function getHandlers()
    {
        return $this->handlers;
    }

    /**
     * {@inheritdoc}
     */
    public function getPlaceholder()
    {
        if (!$this->placeholder) {
            return 'newebtime.field_type.currency::input.placeholder';
        }

        return $this->placeholder;
    }

    /**
     * {@inheritdoc}
     */
    public function getInputView()
    {
        if ($view = parent::getInputView()) {
            return $view;
        }

        return 'newebtime.field_type.currency::dropdown';
    }

    /**
     * {@inheritdoc}
     */
    public function getClass()
    {
        if ($class = parent::getClass()) {
            return $class;
        }

        return 'custom-select form-control';
    }

    /**
     * Implode array options into a string
     * so that they can be edited in the CP.
     *
     * @param array $config
     */
    protected function implodeOptions(array &$config)
    {
        if (isset($config['options']) && is_array($config['options'])) {
            array_walk(
                $config['options'],
                function (&$value, $key) {
                    return $value = $key . ': ' . $value;
                }
            );

            $config['options'] = implode("\n", $config['options']);
        }
    }
}
