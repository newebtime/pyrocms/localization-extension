<?php

namespace Newebtime\CurrencyFieldType;

use Anomaly\Streams\Platform\Addon\FieldType\FieldTypePresenter;
use Propaganistas\LaravelIntl\Facades\Currency;

/**
 * Class CurrencyFieldTypePresenter
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class CurrencyFieldTypePresenter extends FieldTypePresenter
{
    /**
     * Return the currency name.
     *
     * @return string
     */
    public function name()
    {
        if (!$currency = $this->object->getValue()) {
            $currency = config('streams::currencies.default');
        }

        return Currency::name($currency);
    }

    /**
     * Return the currency symbol.
     *
     * @return string
     */
    public function symbol()
    {
        if (!$currency = $this->object->getValue()) {
            $currency = config('streams::currencies.default');
        }

        return Currency::symbol($currency);
    }
}
