<?php

return [
    'except' => [
        'type' => 'anomaly.field_type.text',
    ],
    'only'   => [
        'type' => 'anomaly.field_type.text',
    ],
];
