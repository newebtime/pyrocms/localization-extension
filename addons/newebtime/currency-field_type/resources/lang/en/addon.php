<?php

return [
    'title'       => 'Currency',
    'name'        => 'Currency Field Type',
    'description' => 'A currency field type'
];
