<?php

return [
    'except' => [
        'label' => 'Except Currency',
    ],
    'only'   => [
        'label' => 'Only Currency',
    ]
];
