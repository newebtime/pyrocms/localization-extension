<?php

namespace Newebtime\MoneyFieldType;

use Anomaly\Streams\Platform\Addon\FieldType\FieldTypeSchema;
use Anomaly\Streams\Platform\Assignment\Contract\AssignmentInterface;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Fluent;
use Illuminate\Support\Str;

/**
 * Class MoneyFieldTypeSchema
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class MoneyFieldTypeSchema extends FieldTypeSchema
{

    /**
     * @param Blueprint           $table
     * @param AssignmentInterface $assignment
     */
    public function addColumn(Blueprint $table, AssignmentInterface $assignment)
    {
        // Skip if the column already exists.
        if ($this->schema->hasColumn($table->getTable(), $this->fieldType->getColumnName())) {
            return;
        }

        /**
         * Add the column to the table.
         *
         * @var Blueprint|Fluent $column
         */
        $column = $table->{$this->fieldType->getColumnType()}(
            $this->fieldType->getColumnName(),
            false,
            false
        )->nullable(!$assignment->isTranslatable() ? !$assignment->isRequired() : true);

        if (!Str::contains($this->fieldType->getColumnType(), ['text', 'blob'])) {
            $column->default(false);
        }

        // Mark the column unique if desired and not translatable.
        if ($assignment->isUnique() && !$assignment->isTranslatable()) {
            $table->unique($this->fieldType->getColumnName());
        }
    }
}
