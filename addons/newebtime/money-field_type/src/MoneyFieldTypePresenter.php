<?php

namespace Newebtime\MoneyFieldType;

use Anomaly\Streams\Platform\Addon\FieldType\FieldTypePresenter;
use Propaganistas\LaravelIntl\Facades\Currency;
use Propaganistas\LaravelIntl\Facades\Number;

/**
 * Class MoneyFieldTypePresenter
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class MoneyFieldTypePresenter extends FieldTypePresenter
{
    /**
     * Return the formatted decimal.
     *
     * @return string
     */
    public function format()
    {
        return Number::format($this->object->getValue());
    }

    /**
     * Return the integer formatted as a currency.
     *
     * @param null   $currency
     * @param string $field
     * @return string
     */
    public function currency($currency = null, $field = 'currency')
    {
        if (!$currency) {
            $currency = $this->object->getEntry()->{$field};
        }

        if (!$currency) {
            $currency = config('streams::currencies.default');
        }

        return Currency::format($this->object->getValue(), $currency);
    }
}
