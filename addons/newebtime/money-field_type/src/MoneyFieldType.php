<?php

namespace Newebtime\MoneyFieldType;

use Anomaly\Streams\Platform\Addon\FieldType\FieldType;

/**
 * Class MoneyFieldType
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class MoneyFieldType extends FieldType
{
    /**
     * {@inheritdoc}
     */
    protected $columnType = 'float';

    /**
     * {@inheritdoc}
     */
    protected $inputView = 'newebtime.field_type.money::input';

    /**
     * {@inheritdoc}
     */
    protected $rules = [
        'numeric',
    ];

    /**
     * {@inheritdoc}
     */
    public function getRules()
    {
        $rules = parent::getRules();

        if (!is_null($min = array_get($this->config, 'min'))) {
            $rules[] = 'min:' . $min;
        }

        if (!is_null($max = array_get($this->config, 'max'))) {
            $rules[] = 'max:' . $max;
        }

        return $rules;
    }
}
