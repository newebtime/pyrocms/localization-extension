<?php

return [
    'min'           => [
        'type' => 'anomaly.field_type.text',
    ],
    'max'           => [
        'type'  => 'anomaly.field_type.text',
        'rules' => [
            'numeric',
        ],
    ],
    'default_value' => [
        'type'  => 'anomaly.field_type.text',
        'rules' => [
            'numeric',
        ],
    ],
];
