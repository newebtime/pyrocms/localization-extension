<?php

return [
    'title'       => 'Money',
    'name'        => 'Money Field Type',
    'description' => 'A money field type.'
];
