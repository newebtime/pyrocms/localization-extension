<?php

return [
    'min'           => [
        'label'        => 'Minimum Value',
        'instructions' => 'What is the minimum value allowed?'
    ],
    'max'           => [
        'label'        => 'Maximum Value',
        'instructions' => 'What is the maximum value allowed?'
    ],
    'default_value' => [
        'label'        => 'Default Value',
        'instructions' => 'Enter the default value if any.'
    ]
];
