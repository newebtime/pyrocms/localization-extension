# Localization Extension

Intl extension for PyroCMS and Streams Platform. Implementing `propaganistas/laravel-intl`.

## Installation

`composer required newebtime/localization-extension`

## Provide

* All `propaganistas/laravel-intl` features are available.
* Implement Twig functions: `country`, `currency`, `language`, `number`.

See [`propaganistas/laravel-intl`](https://github.com/Propaganistas/Laravel-Intl) for Usage.

## Addon

The extension come with 2 bundle addons

* `money-field_type`
  * Similar to the core `decimal-field_type`, but using the localization instead of field configuration.
* `currency-field_type`
  * Similar to the core `select-field_type` with `Currency` handler, but using the localization currencies.
