<?php

namespace Newebtime\LocalizationExtension;

use Anomaly\Streams\Platform\Addon\Plugin\Plugin;

/**
 * Class LocalizationExtensionPlugin
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class LocalizationExtensionPlugin extends Plugin
{
    /**
     * Get the plugin functions.
     *
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction(
                'country',
                function ($countryCode = null) {
                    if (is_null($countryCode)) {
                        return app('intl.country');
                    }

                    return app('intl.country')->name($countryCode);
                }
            ),
            new \Twig_SimpleFunction(
                'currency',
                function () {
                    $arguments = func_get_args();

                    if (count($arguments) === 0) {
                        return app('intl.currency');
                    }

                    if (count($arguments) > 0 && is_numeric($arguments[0])) {
                        return app('intl.currency')->format(...$arguments);
                    }

                    return app('intl.currency')->name(...$arguments);
                }
            ),
            new \Twig_SimpleFunction(
                'language',
                function ($langCode = null) {
                    if (is_null($langCode)) {
                        return app('intl.language');
                    }

                    return app('intl.language')->name($langCode);
                }
            ),
            new \Twig_SimpleFunction(
                'number',
                function ($number = null, $options = []) {
                    if (is_null($number)) {
                        return app('intl.number');
                    }

                    return app('intl.number')->format($number, $options);
                }
            ),
        ];
    }
}
