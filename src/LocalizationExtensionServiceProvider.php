<?php

namespace Newebtime\LocalizationExtension;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;
use Propaganistas\LaravelIntl\IntlServiceProvider;

/**
 * Class LocalizationExtensionServiceProvider
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class LocalizationExtensionServiceProvider extends AddonServiceProvider
{
    /**
     * {@inheritdoc}
     */
    protected $plugins = [
        LocalizationExtensionPlugin::class,
    ];

    /**
     * {@inheritdoc}
     */
    protected $providers = [
        IntlServiceProvider::class,
    ];
}
