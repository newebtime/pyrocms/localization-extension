<?php

namespace Newebtime\LocalizationExtension;

use Anomaly\Streams\Platform\Addon\Extension\Extension;
use Newebtime\CurrencyFieldType\CurrencyFieldType;
use Newebtime\MoneyFieldType\MoneyFieldType;

/**
 * Class LocalizationExtension
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class LocalizationExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    protected $addons = [
        MoneyFieldType::class,
        CurrencyFieldType::class
    ];
}
