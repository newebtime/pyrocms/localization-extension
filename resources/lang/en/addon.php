<?php

return [
    'title'       => 'Localization',
    'name'        => 'Localization Extension',
    'description' => 'Intl extension for PyroCMS and Streams Platform'
];
